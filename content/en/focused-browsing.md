---
title: Focused Browsing
description: Reclaim your focus with a web extension that hides distracting feeds!
category: App Collection
position: 9
---

## Focusing in the era of news feeds is a challenge

Professionals and creators on the Internet are drowning in information.

This hinders them from doing their best work because their ability to focus is affected by the content and news on their feed.

Currently, platforms like LinkedIn and Twitter do not allow their users to hide their news and content feeds.

But what if we offered them a way to regain control over the feeds they did or did not want to consume?

That is why we're building Focused Browsing.

![Focused Browsing Promo](/focused-browsing/promo.png)

**Reclaim your focus with a web extension that hides distracting feeds!**

## Features

### Show & Hide distractions without leaving the tab you're on.

We want to empower you to focus when you want to, but we also don't want to make it tedious for you when you want to browse what's happening on the Internet.

That is why we made it easy for you to hide and bring back feeds without leaving your tab.

![Screenshot showcasing hiding distractions without leaving the tab you're on](/focused-browsing/screenshot-1.png)

### Control Distractions using keyboard shortcuts

We added keyboard shortcuts to make it seamless and intuitive for you to toggle distractions on our supported websites.

`Shift + F + B` currently toggles all distractions, and we are thinking of other shortcuts to help optimize & personalize your experience.

![Screenshot showcasing controlling focus using keyboard shortcuts](/focused-browsing/screenshot-2.png)

### We support Dim and Dark modes

We added dim and dark mode support for Twitter because we wanted our extension to fit seamlessly with someone's browsing experience on the site.

![Screenshot showcasing Dim and Dark mode support](/focused-browsing/screenshot-3.png)

## Try Focused Browsing Today!

You can download the latest release of Focused Browsing by clicking the button below

<cta-button link="https://github.com/grey-software/focused-browsing/releases/download/v0.9.2-1/focused-browsing-0.9.2-1.zip" text="Focused Browsing v0.9.2-1"></cta-button>

## Compatibility Chart

### Legend

**✅ Passed**
**❗️Unsupported**
**❓Untested**

| Operating System | ![Brave](https://raw.githubusercontent.com/alrra/browser-logos/master/src/brave/brave_24x24.png) | ![Chrome](https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_24x24.png) | ![Firefox](https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_24x24.png) | ![Edge](https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_24x24.png) |
| ---------------- | ------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------- |
| OSX 11.4 (20F71) | 1.26.74 ✅                                                                                       | 91.0.4472.114 ✅                                                                                    | ❗️                                                                                                    | ❓                                                                                            |

<br>

### Running the extension

<alert type="warning">At the moment, Focused Browsing is only compatible with Google Chrome or Brave Browser. If you use Firefox, we invite you to download [News Feed Eradicator](https://addons.mozilla.org/en-US/firefox/addon/news-feed-eradicator/) while we work on Firefox support.</alert>

**The image below** from the [chrome documentation](https://developer.chrome.com/docs/extensions/mv3/getstarted/), **shows how to load an unpublished extension** using developer mode.

<img width="1157" alt="Screen Shot 2021-05-31 at 2 08 10 PM" src="https://user-images.githubusercontent.com/20130700/120519269-6add3e00-c3a0-11eb-9359-ac43efd68733.png">

<alert>After clicking **LOAD UNPACKED**, look for the un-zipped extension directory named `focused-browsing-v0.x` </alert>

<alert type="success">**By the end you should see an extension card that looks like this**
<img style="margin-top:16px"  width="1157" alt="Screen Shot 2021-05-31 at 2 08 10 PM" src="https://i.imgur.com/VK3HoXy.png">
</alert>

<br/>

## Project Status

This project is currently being maintained by [Arsala](https://gitlab.com/ArsalaBangash), [Avi](https://gitlab.com/daveavi), and [Raj](https://gitlab.com/teccUI).

## Credits and Gratitude

### News Feed Eradicator by Jordan West

Focused Browsing originally started as two separate student projects called Twitter Focus and LinkedIn Focus. Both of these were inspired by [News Feed Eradicator for Facebook](News Feed Eradicator for Facebook).

We are sincerely grateful to Jordan for [using the MIT license for NFE](https://github.com/jordwest/news-feed-eradicator). This allowed us to learn from certain aspects of his codebase, and allowed us to use his collection of quotes for our prototypes.

### The open source ecosystem

All software stands upon the foundations laid by the open source world.

We are where we are because of the time, energy, and passion of open source software developers around the world.

We are sincerely grateful for our access to tools that help us create better software.

</br>
