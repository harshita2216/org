---
title: Board of Directors 🧑‍⚖
description: Grey Software's Board of Directors
category: Team
position: 12
boardOfDirectors:
  - name: Arsala
    avatar: https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png
    position: President
    github: https://github.com/ArsalaBangash
    gitlab: https://gitlab.com/ArsalaBangash
    linkedin: https://linkedin.com/in/ArsalaBangash
---

<team-profiles :profiles="boardOfDirectors"></team-profiles>
