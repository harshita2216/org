---
title: Volunteers 🙋
description: Grey Software Volunteer's
category: Team
postion: 10
volunteers:
  - name: Milind
    avatar: https://avatars.githubusercontent.com/u/48028572?s=460&u=7a0793800536db2a99dace6279495835f0550567&v=4
    position: Software Developer
    github: https://github.com/milindvishnoi
    gitlab: https://gitlab.com/milindvishnoi
    linkedin: https://www.linkedin.com/in/milindvishnoi/
  - name: Isha
    avatar: https://avatars.githubusercontent.com/u/56453971?s=460&v=4
    position: Operations
    github: https://github.com/ishaaa-ai
    gitlab: https://gitlab.com/ishaaa-ai
    linkedin: https://www.linkedin.com/in/isha-kerpal-6a923819b/
  - name: Hamees
    avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/8058458/avatar.png
    position: Administrative Officer
    github: https://github.com/mhamees
    gitlab: https://gitlab.com/mhamees
    linkedin: https://www.linkedin.com/in/muhammad-hamees/
  - name: Khadija
    avatar: https://secure.gravatar.com/avatar/0edce69825fe6283e691f77650b44965?s=800&d=identicon
    position: Organization Developer
    github: https://github.com/khadijagardezi
    gitlab: https://gitlab.com/khadijagardezi
    linkedin: https://www.linkedin.com/in/khadijagardezi/

  - name: Yousef
    avatar: https://avatars.githubusercontent.com/u/44936505?v=4
    position: Software Developer
    github: https://github.com/yousefakiba
    gitlab: https://gitlab.com/yousefakiba
    linkedin: https://ca.linkedin.com/in/yousef-akiba-5076b9198
---

<team-profiles :profiles="volunteers"></team-profiles>
